# Herramientas TIC para Mejorar tu Presencia Profesional, Crear un CV de Calidad y Facilitar el Desarrollo de Proyectos

En este Workshop, veremos de manera general las tecnologías que podemos implementar en la ejecución de nuestras tareas diarias, tanto en el área educativa como en el área laboral.

- [Herramientas TIC para Mejorar tu Presencia Profesional, Crear un CV de Calidad y Facilitar el Desarrollo de Proyectos](#herramientas-tic-para-mejorar-tu-presencia-profesional-crear-un-cv-de-calidad-y-facilitar-el-desarrollo-de-proyectos)
  - [Introducción a las TICs que veremos en el curso](#introducci%C3%B3n-a-las-tics-que-veremos-en-el-curso)
    - [Herramientas para comunicación y planeación de proyectos](#herramientas-para-comunicaci%C3%B3n-y-planeaci%C3%B3n-de-proyectos)
    - [Sistema de Control de Versiones](#sistema-de-control-de-versiones)
    - [Herramientas para transmitir información a través de la web](#herramientas-para-transmitir-informaci%C3%B3n-a-trav%C3%A9s-de-la-web)
    - [Editores de Texto](#editores-de-texto)
    - [Diseño de un CV / Portafolio para publicarlo en la web](#dise%C3%B1o-de-un-cv--portafolio-para-publicarlo-en-la-web)

## Introducción a las TICs que veremos en el curso

### Herramientas para comunicación y planeación de proyectos

1. Slack
2. Trello
3. Asana

### Sistema de Control de Versiones

1. Terminal de línea de comandos
2. Git
3. Gitlab / Github

### Herramientas para transmitir información a través de la web

1. HTML (HyperText Markup Language)
2. Markdown

### Editores de Texto

1. VSCode

### Diseño de un CV / Portafolio para publicarlo en la web

1. [El inspector de Chrome](https://developers.google.com/web/tools/chrome-devtools/?hl=es)
2. [VSCode: Conoce el editor de textos](https://code.visualstudio.com/docs/introvideos/basics)
3. [Terminal de línea de comandos](https://ubuntuforums.org/showthread.php?t=1202583)
   1. Comandos básicos
      1. `mkdir`
      2. `touch`
      3. `pwd`
      4. `ls -la`
      5. `more`
      6. `cat`
      7. `tail`
      8. `nano` o `vim`
      9. `open`
      10. `cd`
      11. `cp`
      12. `mv`
      13. `rm`
      14. `rm -rf`
      15. `sudo`
4. [Git](http://rogerdudler.github.io/git-guide/index.es.html)
   1. [First time configuration](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)
   2. Comandos básicos
      1. `git config --list --show-origin`
      2. `git config --list`
      3. `git init`
      4. `git status`
      5. `git add`
      6. `git commit`
      7. `git pull origin master`
      8. `git push -u origin master`
   3. Algunos avanzados
      1. `git commit --amend`
      2. `git log`
      3. `git reflog`
      4. `git reset --hard <commit>`
5. [Gitlab](https://about.gitlab.com/)
   1. [Create Account](https://gitlab.com/users/sign_in#register-pane)
   2. [SSH Key](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)
   3. Tipos de desarrollo
   4. Planificación
      1. [Issues](https://docs.gitlab.com/ee/user/project/issues/)
      2. [Labels](https://docs.gitlab.com/ee/user/project/labels.html)
      3. [Weight](https://docs.gitlab.com/ee/workflow/issue_weight.html#issue-weight-starter)
      4. [Milestones](https://docs.gitlab.com/ee/user/project/milestones/)
      5. [Boards](https://docs.gitlab.com/ee/user/project/issue_board.html)
      6. [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html#service-desk-premium)
      7. [Quick Actions](https://docs.gitlab.com/ee/user/project/quick_actions.html#gitlab-quick-actions)
      8. [Repositories](https://docs.gitlab.com/ee/user/project/repository/)
6. [NVM](https://github.com/nvm-sh/nvm)
7. [ParcelJS](https://parceljs.org/getting_started.html)
8. [HTML](https://developer.mozilla.org/es/docs/Learn/HTML)
9. [CSS](https://developer.mozilla.org/es/docs/Web/CSS)
10. [Formspree](https://formspree.io/plans)
11. [Gitlab Pages](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/)
    1. [Freenom Domain](https://www.freenom.com/en/freeandpaiddomains.html)
    2. [Cloudflare](https://www.cloudflare.com/es-es/)
    3. [Gitlab y Cloudflare](https://about.gitlab.com/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/)
12. Estudiar desarrollo de software
    1. [Platzi](https://platzi.com/c/kata/)
    2. [Udemy](https://www.udemy.com/the-web-developer-bootcamp/)
    3. Modelos de trabajo
    4. Salarios
13. Conclusiones